import React from 'react';
import './Problem1.css';

class Problem1 extends React.Component {



  constructor(props) {
  	super(props);

    this.state = {
    	num1: 0,
    	num2: 0,
    	num3: 0
    };

    this.printMultiples = this.printMultiples.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }



  printMultiples(num1, num2, num3) {
  	var output = '';

  	for (var c = 1; c <= num3; c++) {
    	if (c % num1 == 0 && c % num2 == 0) {
      		output += c + ' ';
    	}
  	}

    alert('The multiples of ' + num1 + ' and ' + num2 + ' are \n\n' + output);
  }



  handleChange(event) {
  	this.setState({[event.target.name]: event.target.value});
  }



  handleSubmit(event) {
  	this.printMultiples(this.state.num1, this.state.num2, this.state.num3);
  	event.preventDefault();
  }



  render() {

  	return (

  		<div class="main">
  			<h1>COMMON MULTIPLES</h1>
        <p>This section will list all common multiples of the <b>first number</b> and the <b>seconds number</b> until the <b>third number</b>.</p>

        <form onSubmit={this.handleSubmit}>
  				<label>
  					First number<br/>
  					<input 
              class="input"
              type="number" 
              name="num1" 
              onChange={this.handleChange}/>
  				</label><br/>

  				<label>
  					Second number<br/>
  					<input 
              class="input"
              type="number" 
              name="num2" 
              onChange={this.handleChange}/>
  				</label><br/>

  				<label>
  					Third number<br/>
  					<input 
              class="input"
              type="number" 
              name="num3" 
              onChange={this.handleChange}/>
  				</label><br/><br/>

  				<input 
            type="submit" 
            class="button"
            value="SUBMIT"/>
  			</form>
  		</div>
  	);
  }
}

export default Problem1;
