import React from 'react';
import './Problem4.css';

class Problem4 extends React.Component {



  constructor(props) {
    super(props);

    this.state = {
    	inputStrs: '',
      inputStr: ''
    };

    this.isEmpty = this.isEmpty.bind(this);
    this.printOutput = this.printOutput.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  

  isEmpty(input) {
    return input.trim().length == 0;
  }



  printOutput(inputStrs, inputStr) {
    var inputs = inputStrs.split(' ');
    var input = inputStr.split('');
    var output = '';

    for (var i = 0; i < inputs.length; i++) {
      var compare = inputs[i];

      for (var j = 0; j < input.length; j++) {
        if (compare.includes(input[j])) {
          input.pop(input[j]);
          output += compare + ' ';
          break;
        }
      }

    }

    alert(output);
  }



  handleChange(event) {
  	this.setState({[event.target.name]: event.target.value});
  }



  handleSubmit(event) {
    this.printOutput(this.state.inputStrs, this.state.inputStr);
  	event.preventDefault();
  }



  render() {

    return (

    	<div class="main">
    		<h1>CHARACTER FINDER</h1>
        <p>This section will list all <b>words</b> that have the same caracter on the <b>2nd word</b>.</p>
        <form onSubmit={this.handleSubmit}>

          <label>
            Words<br/>
            <input 
              class="input"
              type="text" 
              name="inputStrs" 
              onChange={this.handleChange}/>
          </label><br/>

          <label>
            Word<br/>
            <input 
              class="input"
              type="text" 
              name="inputStr" 
              onChange={this.handleChange}/>
          </label><br/><br/>

          <input 
            class="button"
            type="submit" 
            disabled={
              this.isEmpty(this.state.inputStrs) ||
              this.isEmpty(this.state.inputStr)
            } 
            value="Submit"/>
        </form>
    	</div>

    );
  }
}

export default Problem4;
