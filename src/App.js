import React, { Component }from 'react';
import logo from './logo.svg';
import './App.css';

function getMultiples() {
  var num1 = document.GetElementBy("num1Field").value;
  var num2 = document.GetElementBy("num2Field").value;
  var output = "";

  for (var c = 1; c <= 30; c++)
    if (c % num1 == 0 && c % num2 == 0)
      output = output + c + " ";
      
  document.getElementById("outputText").innerHTML = output;
}

function App()  {
  return (

    <div>
      <h1>Common Multiples</h1>

    
        Number 1
        <input 
          type = "number" 
          name = "number1" 
          id = "number1" 
          placeholder = "1st Number"/><br/><br/>

        Number 2
        <input 
          type = "number" 
          name = "number2" 
          id = "number2" 
          placeholder = "2nd Number"/><br/><br/>

        <button onclick = "getMultiples()" >
          GET MULTIPLE
        </button>

      <p id = "outputText">Output</p>

    </div>
  );
}

export default App;
