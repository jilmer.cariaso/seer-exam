import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Problem1 from './problem1/Problem1';
import Problem2 from './problem2/Problem2';
import Problem3 from './problem3/Problem3';
import Problem4 from './problem4/Problem4';
import * as serviceWorker from './serviceWorker';

ReactDOM.render(<Problem1 />, document.getElementById('problem1'));
ReactDOM.render(<Problem2 />, document.getElementById('problem2'));
ReactDOM.render(<Problem3 />, document.getElementById('problem3'));
ReactDOM.render(<Problem4 />, document.getElementById('problem4'));


// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
