import React from 'react';
import './Problem2.css';

class Problem2 extends React.Component {



  constructor(props) {
    super(props);

    this.state = {
    	inputStr: '',
    	num: 0,
    };

    this.isEmpty = this.isEmpty.bind(this);
    this.isValid = this.isValid.bind(this);
    this.printOutput = this.printOutput.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }



  isEmpty(input) {
    return input.trim().length == 0;
  }



  isValid(inputStr) {
  	var format = /[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?qwertyuiopasdfghjklzxcvbnm]/;

    return !format.test(inputStr);
  }



  printOutput(input, num) {
    var output = [];
    var outputStr = '';

    for (var i = 0; i < input.length; i++) {
      for (var j = i + 1; j < input.length; j++) {
        if (input[i] + input[j] == num && 
          !output.includes(input[i]) &&
          !output.includes(input[j])) {
            output.push(input[i]);
            output.push(input[j]);
            outputStr += '{' + input[i] + ', ' + input[j] + '} ';
        }
      }
    }

    alert('All possible addends for number ' + num + ':\n' + outputStr);
  }



  handleChange(event) {
  	this.setState({[event.target.name]: event.target.value});
  }



  handleSubmit(event) {
    var input = this.state.inputStr.split(' ').map(function(item) { return parseInt(item, 10); });
    this.printOutput(input, this.state.num);
  	event.preventDefault();
  }



  render() {

    return (

    	<div class="main">
    		<h1>FIND ADDENDS</h1>
        <p>This section will find all <b>possible addends</b> which sum is the same with the <b>target sum</b>.</p>
    		<form onSubmit={this.handleSubmit}>

    			<label>
    				Possible addends<br/>
    				<input 
              class="input"
              type="text" 
              name="inputStr" 
              onChange={this.handleChange}/>
    			</label><br/>

          <label>
            Target sum<br/>
            <input 
              class="input"
              type="number" 
              name="num" 
              onChange={this.handleChange}/>
          </label><br/><br/>

    			<input 
            type="submit" 
            class="button"
            disabled={
              !this.isValid(this.state.inputStr) || 
              this.isEmpty(this.state.inputStr)} 
            value="Submit"/>
    		</form>
    	</div>

    );
  }
}

export default Problem2;
